# rcom-nn-utils
recommender engine utils and metrics for deep neural network models.

##Installation
```bash
pip install git+https://peeranat85@bitbucket.org/truedmp/rcomnnutils.git@tag
```
where `tag` is a tag of a git commit. With a tag specified, a commit with that tag will be 
installed. If not specified, the latest will be installed.

###Job stats example usage
```buildoutcfg
analyze_df(app_name="RCOM_MODEL_NAME", name="python_module", white_list=dict(), 
           n=10, (("df_name1", df1), ("df_name2", df2))
```


##Versions
* Version 1.1.1
    - add NDCG for vector version (Cosine and Euclidean)
    - add job_stat for summarize dataframe results (DMPREC-580)
    - update job stat result to Applog, import rcomloggingutils Release-1.0.3 (DMPREC-580)

* Version 0.1.0
    - update TensorFlow version to 1.11.0.
    - update utils.

* Version 0.0.1
    - first stable version with utils and metrics modules.
