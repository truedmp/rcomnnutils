from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'tensorflow==1.11.0',
    'scikit-learn==0.20.0',
    'pandas==0.23.4',
    'h5py==2.8.0',
    'scipy==1.1.0'
]

setup(name='rcom-nn-utils',
      version='0.1.0',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcomnnutils',
      include_package_data=True,
      description='rcom-nn-utils',
      author='Patcharin Cheng',
      author_email='patcharin.che@truedigital.com',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)

