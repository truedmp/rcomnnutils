import os
import math
import threading
import pandas as pd
from abc import ABC, abstractmethod

import tensorflow as tf
from tensorflow.python.lib.io import file_io
from tensorflow.python.keras.callbacks import ModelCheckpoint


class threadsafe_iter:
    """Takes an iterator/generator and makes it thread-safe by
    serializing call to the `_next_` method of given iterator/generator.
    """

    def __init__(self, it):
        self.it = it
        self.lock = threading.Lock()

    def __iter__(self):
        return self

    def __next__(self):
        with self.lock:
            return next(self.it)


def threadsafe_generator(f):
    """A decorator that takes a generator function and makes it thread-safe.
    """
    def g(*a, **kw):
        return threadsafe_iter(f(*a, **kw))

    return g


def pad_string_tensor(x, maxlen):
    """converts a string representation of array and left-pad with maximum length of "maxlen" with an empty string ''.
    For example,

    x = tf.constant("[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]")
    pad_string_tensor(x, 5) // returns [b'', b'', b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg']
    pad_string_tensor(x, 2) // returns [b'Nm6mdRQmPMW', b'MdXNpLQe3AOg']

    Parameters
    ----------
    x : string representation of an array
        e.g. "[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg,4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]"

    maxlen : maximum length of output array

    Returns
    -------
    out : array of bytes, padded with a byte of empty string b''.
    """
    tensor = string_to_tensor(x)
    padded = tf.pad(tensor, [[0, 0], [maxlen, 0]])
    sliced = padded[:, -maxlen:]
    return sliced[0]


def string_to_tensor(x):
    """converts a string representation of array to an array of bytes,

    x = tf.constant("[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]")
    string_to_tensor(x) // returns [b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg']

    Parameters
    ----------
    x : string representation of array
        e.g. "[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg,4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]"

    Returns
    -------
    out : array of bytes
    """
    length = tf.size(tf.string_split([x], ""))
    sub = tf.substr(x, 1, length - 2)  # remove the leading '[' and trailing ']'
    splits = tf.string_split([sub], delimiter=',')
    dense = tf.sparse_tensor_to_dense(splits, '')
    return dense


class GcsModelCheckpoint(ModelCheckpoint):
    """
    A Keras callback that saves the model after every epoch to Google Cloud Storage(GCS).

    `gcs_file` can contain named formatting options, which will be filled the value of `epoch` and keys in `logs`
    (passed in `on_epoch_end`). It must prefix with `gs://`. Consider using Keras' `ModelCheckpoint` if your model
    is not saved in GCS.

    For example: if `gcs_file` is `weights.{epoch:02d}-{val_loss:.2f}.hdf5`, then the model checkpoints will be
    saved with the epoch number and the validation loss in the filename.
    """

    def __init__(self, gcs_file, **kwargs):
        checkpoint_dir = 'checkpoints'
        tf.gfile.MakeDirs(checkpoint_dir)
        self.gcs_file = gcs_file
        self.localfile = os.path.join(checkpoint_dir, gcs_file.split('/')[-1])
        super().__init__(self.localfile, **kwargs)

    def on_epoch_end(self, epoch, logs=None):
        """
        implements Keras' ModelCheckpoint's `on_epoch_end`. This method saves model in local storage first
        under `checkpoints` directory and then the model is copied to GCS path as specified in `gcs_file` immediately.
        """
        super().on_epoch_end(epoch, logs)

        local_model = self.localfile.format(epoch=epoch+1, **logs)
        gcs_model = self.gcs_file.format(epoch=epoch+1, **logs)

        if file_io.file_exists(local_model):
            with file_io.FileIO(local_model, mode='rb') as input_f:
                with file_io.FileIO(gcs_model, mode='w+') as output_f:
                    output_f.write(input_f.read())


class BaseDataGenerator(ABC):
    """A base class for reading CSV files in chunks.

    Parameters
    ----------
    directory : str
        a directory in which all data are located. Data are in CSV file format.

    batch_size : int, default 1024
        batch size of the data

    chunk_size : int, default 20000
        a number of rows to read from CSV file in batch.
    """
    def __init__(self, directory, batch_size=1024, chunk_size=20000):
        self.batch_size = batch_size
        self.chunk_size = chunk_size
        self.files = tf.gfile.Glob(directory + '/*.csv')

    def _get_file(self, file):
        if file.startswith('gs://'):
            return tf.gfile.Open(file, mode='rb')
        return file

    def get_data(self, fields, **kwargs):
        """returns data in a given list of fields.

        Parameters
        ----------
        fields : list<str>
            a list of field names to return

        Returns
        -------
        data : array
        """
        dfs = []
        for file in self.files:
            df = pd.read_csv(self._get_file(file), chunksize=self.chunk_size, **kwargs)
            for df_chunk in df:
                chunk = df_chunk[fields]
                dfs.append(chunk)

        return pd.concat(dfs, ignore_index=True).values

    @abstractmethod
    def steps_per_epoch(self, **kwargs):
        raise NotImplementedError("steps_per_epoch() not implemented")

    @abstractmethod
    def generate(self, *args, **kwargs):
        raise NotImplementedError("generate() not implemented")


class BaseTFCsvDataGenerator(BaseDataGenerator):
    """Base CSV file data generator that makes use of TensorFlow data API.

    Parameters
    ----------
    directory : str
        a directory in which all data are located. Data are in CSV file format.

    record_defaults : list of default values
        list of default values of each column

    batch_size : int, default 1024
        batch size of the data

    chunk_size : int, default 20000
        a number of rows to read from CSV file in batch.
    """
    def __init__(self, directory, record_defaults, batch_size=1024, chunk_size=20000):
        super().__init__(directory, batch_size, chunk_size)
        self.record_defaults = record_defaults

    @abstractmethod
    def preprocess(self, *args):
        """returns a dictionary of features and their corresponding targets.

        Parameters
        ----------
        args : fields corresponding to columns in a CSV file

        Returns
        -------
        (features, targets) : (dict, array)
            features is a dictionary of features
            targets is an array of targets corresponding to the features
        """
        raise NotImplementedError("preprocess() not implemented")

    def steps_per_epoch(self, header=True):
        """The total number of steps (batches of samples) to yield from generator before declaring one epoch
        finished and starting the next epoch.
        """
        dataset = (tf.contrib.data.CsvDataset(self.files, self.record_defaults, header=header)
                   .batch(batch_size=self.batch_size, drop_remainder=False))

        it = dataset.make_one_shot_iterator()
        next_batch = it.get_next()
        records = 0
        with tf.Session() as sess:
            while True:
                try:
                    batch = sess.run(next_batch)
                    records += len(batch[0])
                except tf.errors.OutOfRangeError:
                    break
        return math.ceil(records / self.batch_size)

    def generate(self, header=True, is_train=False, shuffle_buffer=5000, num_parallel_calls=5):
        """returns an iterator of a dataset in a specified "directory". The iterator returns batches of a specified
        "batch_size" except the last batch of an epoch/CSV file which may be smaller than others.

        Parameters
        ----------
        header : boolean, default True
            whether this dataset contains header or not.

        is_train : boolean, default True
            True will shuffle this dataset, False will not.

        shuffle_buffer : int, default 5000
            a buffer size representing the number of elements from this dataset from which the new dataset will sample.

        num_parallel_calls : int default 5
            represents the number elements to process in parallel. If not specified, elements will be processed
            sequentially.
        """
        dataset = tf.contrib.data.CsvDataset(self.files, self.record_defaults, header=header)

        if is_train:
            dataset = dataset.shuffle(shuffle_buffer)  # depends on sample size

        dataset = (dataset.map(self.preprocess, num_parallel_calls=num_parallel_calls)
                   .batch(batch_size=self.batch_size, drop_remainder=False)
                   .repeat()
                   .prefetch(tf.contrib.data.AUTOTUNE))

        return dataset.make_initializable_iterator()


class BaseCsvDataGenerator(BaseDataGenerator):
    """Base CSV file data generator that makes use of Pandas.
    """

    def __init__(self, directory, batch_size=1024, chunk_size=20000):
        super().__init__(directory, batch_size, chunk_size)

    @abstractmethod
    def preprocess(self, df):
        """returns a batch of training samples and their corresponding targets.

        Parameters
        ----------
        df : pandas dataframe

        Returns
        -------
        (X, y) : ([n_samples], [n_samples])
            X is an array of training samples,
            y is X's corresponding targets
        """
        raise NotImplementedError("preprocess() not implemented")

    def steps_per_epoch(self, **kwargs):
        """The total number of steps (batches of samples) to yield from generator before declaring one epoch
        finished and starting the next epoch.
        """
        steps = 0
        for file in self.files:
            df = pd.read_csv(self._get_file(file), chunksize=self.chunk_size, **kwargs)
            for df_chunk in df:
                chunk_steps = math.ceil(len(df_chunk) / self.batch_size)
                steps += chunk_steps
        return steps

    @threadsafe_generator
    def generate(self, **kwargs):
        """generate batches of data located under a specified `directory` indefinitely. The generated batches will have
        equal size as specified in `batch_size` except perhaps the last batch of an epoch/CSV file which may be smaller than
        others. This method is thread-safe.
        """
        while True:
            for file in self.files:
                df = pd.read_csv(self._get_file(file), chunksize=self.chunk_size, **kwargs)
                for df_chunk in df:
                    chunk_steps = math.ceil(len(df_chunk) / self.batch_size)
                    for i in range(chunk_steps):
                        batch = df_chunk[i * self.batch_size:(i + 1) * self.batch_size]
                        X_batch, y_batch = self.preprocess(batch)

                        yield X_batch, y_batch
