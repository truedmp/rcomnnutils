import numpy as np
import tensorflow as tf
from sklearn.utils import check_X_y
from sklearn.preprocessing import LabelBinarizer


def map():
    raise NotImplementedError


def dcg_score(y_true, y_score, k=5):
    """Discounted cumulative gain (DCG) at rank K.
    Modified from https://www.kaggle.com/davidgasquez/ndcg-scorer

    Parameters
    ----------
    y_true : array, shape = [n_samples]
        Ground truth (true relevance labels).
    y_score : array, shape = [n_samples, n_classes]
        Predicted scores.
    k : int
        Rank.

    Returns
    -------
    score : float
    """
    y_score, y_true = check_X_y(y_score, y_true)
    return _dcg_score(y_true, y_score, k)


def _dcg_score(y_true, y_score, k):
    order = np.argsort(y_score)[::-1]
    y_true = np.take(y_true, order[:k])
    gain = 2 ** y_true - 1

    discounts = np.log2(np.arange(len(y_true)) + 2)
    return np.sum(gain / discounts)


def ndcg_score(ground_truth, predictions, k=5):
    """Normalized discounted cumulative gain (NDCG) at rank K.
    Modified from https://www.kaggle.com/davidgasquez/ndcg-scorer

    Normalized Discounted Cumulative Gain (NDCG) measures the performance of a
    recommendation system based on the graded relevance of the recommended
    entities. It varies from 0.0 to 1.0, with 1.0 representing the ideal
    ranking of the entities.

    Parameters
    ----------
    ground_truth : array, shape = [n_samples]
        Ground truth (true labels represended as integers).
    predictions : array, shape = [n_samples, n_classes]
        Predicted probabilities.
    k : int
        Rank.

    Returns
    -------
    score : float

    Example
    -------

    >>> ground_truth = [1, 0, 2]
    >>> predictions = [[0.15, 0.55, 0.2], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
    >>> score = ndcg_score(ground_truth, predictions, k=2)
    1.0
    >>> predictions = [[0.9, 0.5, 0.8], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
    >>> score = ndcg_score(ground_truth, predictions, k=2)
    0.6666666666
    """

    predictions, ground_truth = check_X_y(predictions, ground_truth)
    lb = LabelBinarizer()
    lb.fit(range(len(predictions[0])))
    T = lb.transform(ground_truth)
    scores = []

    # Iterate over each y_true and compute the DCG score
    for y_true, y_score in zip(T, predictions):
        actual = _dcg_score(y_true, y_score, k)
        best = _dcg_score(y_true, y_true, k)
        if best == 0.:
            score = 0.
        else:
            score = float(actual) / float(best)
        scores.append(score)
    return np.mean(scores)


def _log2(x):
    numerator = tf.log(x)
    denominator = tf.log(tf.constant(2, dtype=numerator.dtype))
    return numerator / denominator


def _keras_compute_dcg(y_true, y_pred, k):
    _, sorted_true_rel_idx = tf.nn.top_k(y_true, k=k)
    max_true_rel_idx = sorted_true_rel_idx[:, 0]
    max_true_rel_idx = tf.expand_dims(max_true_rel_idx, axis=1)
    expanded_max_true_rel_idx = tf.tile(max_true_rel_idx, [1, k])

    # sort predicted relevance scores of each sample and get the sorted indices
    _, sorted_pred_rel_idx = tf.nn.top_k(y_pred, k=k)

    # compare element-wise if the predicted ranking matches with true ranking
    # in the first position only. (we've just duplicated the first true
    # ranking position to other k positions in previous step)
    relevant_scores = tf.cast(
        tf.equal(expanded_max_true_rel_idx, sorted_pred_rel_idx), dtype=tf.float32)

    # compute the gain and discounted scores
    gain = tf.subtract(
        tf.pow(tf.constant(2, dtype=tf.float32), relevant_scores),
        tf.constant(1, dtype=tf.float32))
    positions = tf.constant((np.arange(k) + 2), dtype=tf.float32)
    discounts = _log2(positions)
    dcg = tf.reduce_sum(tf.divide(gain, discounts))
    return dcg


def keras_ndcg(k=5):
    """
    ndcg metric, to be used with Keras or TF to compute NDCG@k given
    tensor ground-truth and prediction inputs.

    Parameters
    ----------
        k : int, default 5
            number of items to be used for computing NDCG. This must be less
            than or equal to the size of recommended items by the model.
    """
    def keras_ndcg_score(y_true, y_pred):
        """
        Custom metric function for Keras's fit( ) and fit_generator( ) to measure NDCG.
        This metric expects the ground truth tensor to have the relevant score of the
        item to be recommended set to 1, and 0 for all other non-relevant items.
        E.g., if there is 1 sample with the item at index 2 being the target of the
        prediction (item to be recommended), then y_true should look like

            y_true = [[0, 0, 1, 0, 0, ..., 0]]

        while the y_pred is the softmax prediction tensor from the model that can take
        on values like the following example

            y_pred = [[0.001, 0.047, 0.015, ..., 0.071]]

        Note that this effectively means this metric somewhat reflects "prediction
        accuracy" of the model to predict the most likely item the user would choose
        while ignoring other predictions in the list. Therefore, this is not the best
        metric for models developed to solve a ranking problem, but more for those
        whose problems have been formulated as classification problem.

        Parameters
        ----------
        y_true : tf.Tensor
            ground-truth tensor of shape (None, N) where N is the number of classes
            and the first dimension represents the batch dimension.
        y_pred : tf.Tensor
            prediction tensor of the same shape as y_true. This is the
            probabilities output of a softmax.
        """
        dcg = _keras_compute_dcg(y_true, y_pred, k)
        ideal_dcg = _keras_compute_dcg(y_true, y_true, k)
        return tf.divide(dcg, ideal_dcg)

    return keras_ndcg_score


# NDCG in vector version
def getNearestIndex(vectors, target, k, distance):
    if distance == 'cosine':
        cosine_distance = tf.reduce_sum(
            tf.multiply(
                tf.nn.l2_normalize(target, axis=-1),
                tf.nn.l2_normalize(vectors, axis=-1)
            ), axis=-1, keepdims=True)

        indices = tf.reshape(cosine_distance, (tf.shape(target)[0], -1))
        return (tf.nn.top_k(indices, k=k))
    else:
        l2_distance = tf.norm(
            vectors - target, ord='euclidean', axis=-1, keepdims=True)
        indices = tf.reshape(l2_distance, (tf.shape(target)[0], -1))
        return (tf.nn.top_k(-indices, k=k))


def _compute_dcg_vector(y_true, y_pred, k, distance, tf_emb):

    # Expand dimension in order to perform matrix calculation
    y_true = tf.expand_dims(y_true, 1)
    y_pred = tf.expand_dims(y_pred, 1)

    # --- Ground truth value ----
    _, sorted_true_rel_idx = getNearestIndex(tf_emb, y_true, k, distance)
    max_true_rel_idx = sorted_true_rel_idx[:, 0]
    max_true_rel_idx = tf.expand_dims(max_true_rel_idx, axis=1)
    expanded_max_true_rel_idx = tf.tile(max_true_rel_idx, [1, k])

    # --- Predicted value ----

    # sort predicted relevance scores of each sample and get the sorted indices
    _, sorted_pred_rel_idx = getNearestIndex(tf_emb, y_pred, k, distance)
    # compare element-wise if the predicted ranking matches with true ranking
    # in the first position only. (we've just duplicated the first true
    # ranking position to other k positions in previous step)
    relevant_scores = tf.cast(
        tf.equal(expanded_max_true_rel_idx, sorted_pred_rel_idx), dtype=tf.float32)

    # compute the gain and discounted scores
    gain = tf.subtract(
        tf.pow(tf.constant(2, dtype=tf.float32), relevant_scores),
        tf.constant(1, dtype=tf.float32))
    positions = tf.constant((np.arange(k) + 2), dtype=tf.float32)
    discounts = _log2(positions)
    dcg = tf.reduce_sum(tf.divide(gain, discounts))
    return dcg


def keras_ndcg_vector(k=5, distance='cosine', target_embedding=[]):
    """
    As same as keras ndcg keras_ndcg_vector
    Instead of using top k max probabilities from output, using vector to
    find the top k nearest vector given pre-defined embedding vectors
    Consist of two version, Euclidean distance and cosine similarity version.

    ndcg metric, to be used with Keras or TF to compute NDCG@k given
    tensor ground-truth and prediction inputs.

    Parameters
    ----------
        k : int, default 5
            number of items to be used for computing NDCG. This must be less
            than or equal to the size of recommended items by the model.

        type : string, default cosine
            how to find the nearest vectors
            cosine : cosine similarity
            euclidean : l2 distance

        embedding vectors : array
            pre-defined embedding vectors to find the nearest vector
    """

    tf_emb = tf.convert_to_tensor(target_embedding, dtype=tf.float32)

    def keras_ndcg_score(y_true, y_pred):
        """
        Custom metric function for Keras's fit( ) and fit_generator( ) to measure NDCG.
        This metric aims to find the index of given vector (ground truth) with pre-defined embedding vectors,

        then y_true should look like (for example, embedding size is 3)

            y_true = [[0, 0, 1],[0, 1, 0]]

        while the y_pred is the predicted tensor vector from the model that can take
        on values like the following example

            y_pred = [[0.1, -0.1, 1], [0, 1, 0]]

        As a result, each vector in y_true will find the nearest vectors and return the indices
        then compare with the y_pred in the same way

        Parameters
        ----------
        y_true : tf.Tensor
            ground-truth tensor of shape (None, N) where N is the number of classes
            and the first dimension represents the batch dimension.
        y_pred : tf.Tensor
            prediction tensor of the same shape as y_true. This is the
            vector.
        """
        dcg = _compute_dcg_vector(y_true, y_pred, k, distance, tf_emb)
        ideal_dcg = _compute_dcg_vector(y_true, y_true, k, distance, tf_emb)
        return tf.divide(dcg, ideal_dcg)

    return keras_ndcg_score


def _mask_top_k(array_to_mask, k=5):
    # Mask array with [0, 1]
    topK = tf.nn.top_k(array_to_mask, k=k)
    min_val = tf.reduce_min(topK.values, axis=1, keepdims=True)
    result = tf.cast(tf.greater_equal(array_to_mask, min_val), tf.float64)
    return result


def micro_f1_score(y_true, y_score, k=5):
    """
    Custom metric function for Keras's fit( ) to measure Micro F1 score.
    This metric expects the ground truth tensor to be set as 1 for correct class, 0 otherwise.
    E.g., if there is 1 sample with the correct labels at index 2, and 3 the target
    of the prediction (tags to be tagged), then y_true should look like.

        y_true = [[0, 0, 1, 1, 0, 0, ..., 0]]

    while the y_pred is the sigmoid prediction tensor from the model that can take
    on values like the following example.

        y_pred = [[0.0012, 0.0032, 0.7521, 0.8567, ..., 0.0034]]

    Note that this effectively means this metric somewhat reflects "prediction
    accuracy" of the model to predict the most likely labels the content should be tagged.

    Parameters
    ----------
        y_true : tf.Tensor
                ground-truth tensor of shape (None, N) where N is the number of classes
                and the first dimension represents the batch dimension.
        y_pred : tf.Tensor
                prediction tensor of the same shape as y_true. This is the
                value output of a sigmoid.
        k      : int
                number of items to be used for computing micro f1 score. This must be less
                than or equal to the size of labels by the model.
    """
    y_pred = tf.cast(y_score, tf.float64)
    y_true = tf.cast(y_true, tf.float64)

    tf.assert_equal(tf.shape(y_true), tf.shape(y_pred),
                    message="Shape is not equal y_true.shape != y_pred.shape")

    y_pred = _mask_top_k(y_pred, k)

    TP = tf.count_nonzero(y_pred * y_true)
    FP = tf.count_nonzero(y_pred * (y_true - 1))
    FN = tf.count_nonzero((y_pred - 1) * y_true)

    precision = TP / (TP + FP)
    recall = TP / (TP + FN)
    f1 = 2 * precision * recall / (precision + recall)
    return f1
