import json
import itertools
import numpy as np
from collections import Counter
from rcomloggingutils.applog_utils import AppLogger


def _flatten_list(df, col_name):
    """
    flatten all items in a column contains list
    :param df: df; dataframe
    :param col_name: 'redeem_type' ; column_name that contains list
    :return: list of all flattened items in the column
    """
    explode_list = [i for i in df[col_name]]
    flatten_list = list(itertools.chain.from_iterable(explode_list))
    return flatten_list


def _get_most_common_in_list(df, col_name, n=10):
    """
    get top most items in the list and calculate total exist item and percentage of each items in the list
    :param df: dataframe
    :param col_name: 'tags'; column that contains list
    :param n: default 10, top most common items
    :return: list of top most items, count of top most items, percentage of top most items
    """
    most_common_item = []
    most_common_value = []
    flatten_list = _flatten_list(df, col_name)
    counter = Counter(flatten_list)

    for name, count in counter.most_common(n):
        most_common_item.append(name)
        most_common_value.append(count)
    most_common_percent = list(np.around(np.divide(most_common_value, len(flatten_list)) * 100, decimals=2))
    return most_common_item, most_common_value, most_common_percent


def _top_value_count(df, col_name, n=10):
    """
    get top most items in the datafrane and calculate total exist item and percentage of each items in the dataframe
    :param df: dataframe
    :param col_name: column that contains category value
    :param n: default 10
    :return: list of top most items, count of top most items, percentage of top most items
    """
    total = df[col_name].count()
    most_common = df[col_name].value_counts().head(n).reset_index()
    most_common_item = list(most_common["index"])
    most_common_value = list(most_common[col_name])
    most_common_percent = list(np.around(np.divide(most_common_value, total) * 100, 2))
    return most_common_item, most_common_value, most_common_percent


def _total_empty_list(df, col_name):
    """
    count total empty list
    :param df: dataframe
    :param col_name: column that contains list
    :return: total empty list
    """
    empty_list = len(df[df[col_name].map(lambda d: len(d)) == 0][col_name])
    return empty_list


def _unexpected_item_in_list(df, col_name, white_list):
    """
    find total count and percentage of unexpected items in the list
    :param df: dataframe
    :param col_name: column that contains list
    :param white_list: json file name
    :return: total count and percentage of unexpected items in the list
    """
    count_unexpected = []
    all_items = _flatten_list(df, col_name)
    unique_item = set(all_items)
    unexpected = list(unique_item - set(white_list[col_name]))
    [count_unexpected.append(Counter(all_items)[i]) for i in unexpected]
    percent_unexpected = list(np.around(np.divide(count_unexpected, len(all_items)) * 100, 2))
    return unexpected, count_unexpected, percent_unexpected


def _unexpected_item_in_df(df, col_name, white_list):
    """
    find total count and percentage of unexpected items in the category column
    :param df: dataframe
    :param col_name: column that contains category value
    :param white_list: json file name
    :return:total count and percentage of unexpected items in the category column
    """
    count_total = df[col_name].count()
    df[col_name].dropna(inplace=True)
    set_unique_item = set(df[col_name].unique())
    unexpected = list(set_unique_item - set(white_list[col_name]))
    count_unexpected = []
    for i in unexpected:
        df_col = df[col_name] == i
        df_col = df_col.value_counts().reset_index()
        count_unexpected.append(df_col[df_col["index"] == True][col_name].values[0])
    percent_unexpected = list(np.around(np.divide(count_unexpected, count_total) * 100, 2))
    return unexpected, count_unexpected, percent_unexpected


def _get_unexpected(df, col_name, n, white_list, dictionary):
    """
    :param df: dataframe
    :param df_name:
    :param col_name:
    :param n: number of top most expected items
    :param white_list: json file name
    :param dictionary: dictionary["unexpectedItems"] for append unexpectedItems
    """
    for key in white_list.keys():
        if key == col_name:
            if isinstance(df[col_name][0], list):
                unexpected, count_unexpected, percent_unexpected = _unexpected_item_in_list(df, col_name, white_list)
            elif df[col_name].dtype == 'object':
                unexpected, count_unexpected, percent_unexpected = _unexpected_item_in_df(df, col_name, white_list)
            else:
                break

            if unexpected != []:
                zipped = sorted(zip(unexpected, count_unexpected, percent_unexpected), key=lambda x: x[1], reverse=True)
                item, count, percent = zip(*zipped)
                dictionary["unexpectedItems"].append(
                    {"name": col_name, "key": list(item)[:n], "count": list(count)[:n], "percent": list(percent)[:n]})


def _get_numeric_stat(df, col_name):
    """
    calculate max, min, std, mean, mode
    :param df: dataframe
    :param col_name: column name that contains numeric values
    :return: max, min, std, mean, mode
    """
    max_item = df[col_name].max()
    min_item = df[col_name].min()
    std_item = round(df[col_name].std(), 2)
    avg_item = round(df[col_name].mean(), 2)
    mode_item = round(df[col_name].mode()[0], 2)
    return max_item, min_item, std_item, avg_item, mode_item


def _generate_dict(df_name, df, n, white_list=dict()):
    """
    generate dictionary that analyze dataframe
    :param df_name: name of dataframe
    :param df: dataframe
    :param n: number of top most common items
    :param white_list_path: path of json file that contains expected value
    :return: dictionary
    """
    column_names = list(df.columns.values)
    total_rows = len(df[column_names[0]])

    dictionary = dict()
    dictionary["df_name"] = df_name
    dictionary["column"] = column_names
    dictionary["row_count"] = total_rows
    dictionary["null"] = []
    dictionary["emptyList"] = []
    dictionary["unexpectedItems"] = []
    dictionary["mean"] = []
    dictionary["max"] = []
    dictionary["min"] = []
    dictionary["std"] = []
    dictionary["mode"] = []
    dictionary["freqItems"] = []
    dictionary["schema"] = []

    for i in column_names:
        if i.find('id') < 0:
            sum_null = df[i].isnull().values.sum()
            if sum_null != 0:
                dictionary["null"].append({"name": i, "value": sum_null})
            if white_list != dict():
                _get_unexpected(df, i, n, white_list, dictionary)
            if isinstance(df[i][0], list):
                sum_empty_list = _total_empty_list(df, i)
                item, value, percent = _get_most_common_in_list(df, i, n)
                dictionary["freqItems"].append({"name": i, "key": item, "count": value, "percent": percent})
                dictionary["emptyList"].append({"name": i, "value": sum_empty_list})
                dictionary["schema"].append({"name": i, "value": "list"})
                break

            elif df[i].dtype != 'object':
                max_item, min_item, std_item, avg_item, mode_item = _get_numeric_stat(df, i)
                dictionary["min"].append({"name": i, "value": min_item})
                dictionary["max"].append({"name": i, "value": max_item})
                dictionary["mean"].append({"name": i, "value": avg_item})
                dictionary["std"].append({"name": i, "value": std_item})
                dictionary["mode"].append({"name": i, "value": mode_item})
                dictionary["schema"].append({"name": i, "value": "numeric"})

            else:
                top_item, top_value, top_percent = _top_value_count(df, i, n)
                dictionary["freqItems"].append(
                    {"name": i, "key": top_item, "count": top_value, "percent": top_percent})
                dictionary["schema"].append({"name": i, "value": "string"})
        else:
            dictionary["schema"].append({"name": i, "value": "string"})
    return dictionary


def _default(obj):
    """
    for handle numpy array in dictionary when converting to json
    """
    if type(obj).__module__ == np.__name__:
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return obj.item()
    raise TypeError('Unknown type:', type(obj))


def _convert_dict_json(dictionary):
    """
    convert dictionary to json
    :param dictionary: python dictionary
    :return: json format
    """
    json_result = json.dumps(dictionary, default=_default)
    json_data = json.loads(json_result)
    return json_data


def analyze_df(app_name="", name="", white_list=dict(), n=10, *args):
    """
    main stat util to call in etl jobs
    :param app_name: name of the application. If not given, it tries to get
                the name from an environment variable RCOM_MODEL_NAME first. If
                that is not set, it uses the default empty string "" as the name.
    :param name: name of this logger (e.g., Python module's name)
    :param args: tuple of dataframe name and dataframe parameters
            usage example: (("df_name1", df1), ("df_name2", df2), ("df_name3", df3))
    """
    _logger = AppLogger(app_name=app_name, name=name)
    for df_name, df in args:
        dictionary = _generate_dict(df_name, df, n, white_list)
        json_data = _convert_dict_json(dictionary)
        _logger.info("analyze job stat", **json_data)

