import os
import pandas as pd
import tensorflow as tf
from dmputils.utils import string_to_tensor, pad_string_tensor, BaseTFCsvDataGenerator, BaseCsvDataGenerator

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))


class TFCsvDataGenerator(BaseTFCsvDataGenerator):

    def __init__(self, directory, record_defaults, batch_size=1024):
        super().__init__(directory, record_defaults, batch_size)

    def preprocess(self, ssoid, view_start, day_of_week, day_period, watch_country, movie_id, watch_actor,
                   watch_category, watch_director, watched):
        features = {
            'watched': pad_string_tensor(watched, 5),
            'day_of_week': [day_of_week],
            'day_period': [day_period],
            'watch_category': pad_string_tensor(watch_category, 5),
            'watch_actor': pad_string_tensor(watch_actor, 5),
            'watch_country': pad_string_tensor(watch_country, 5),
            'watch_director': pad_string_tensor(watch_director, 5),
        }
        return features, [1]


class CsvDataGenerator(BaseCsvDataGenerator):

    def preprocess(self, df):
        X = df['watched'].values
        y = df['movie_id'].values
        return X, y


class BaseDataGeneratorTest(tf.test.TestCase):

    def test_steps_per_epoch_contains_remainder(self):
        with self.test_session():
            record_defaults = [[""]] * 10
            gen = TFCsvDataGenerator(CURRENT_DIR, record_defaults, batch_size=3)
            actual = gen.steps_per_epoch()
            self.assertEqual(2, actual)

    def test_steps_per_epoch_no_remainder(self):
        with self.test_session():
            record_defaults = [[""]] * 10
            gen = TFCsvDataGenerator(CURRENT_DIR, record_defaults, batch_size=5)
            actual = gen.steps_per_epoch()
            self.assertEqual(1, actual)

    def test_get_data(self):
        record_defaults = [[""]] * 10
        gen = TFCsvDataGenerator(CURRENT_DIR, record_defaults, batch_size=4)
        actual = gen.get_data(['movie_id', 'country'])
        exp = [['WGdZ5qZmLw0', '[USA,UK]'], ['B2JO1owWbeLn', '[USA]'], ['oQvJGPZAq8m', '[Korea]'], ['Dm6Z7kZn3MYm', '[USA]'],
               ['E8NMJaXRqGGZ', '[USA]']]
        self.assertAllEqual(exp, actual)


class BaseCsvDataGeneratorTest(tf.test.TestCase):

    def test_preprocess(self):
        gen = CsvDataGenerator(CURRENT_DIR, batch_size=3)
        df = pd.read_csv(CURRENT_DIR + '/data.csv')
        records = gen.preprocess(df)
        features = records[0]
        labels = records[1]
        self.assertAllEqual(['[]', '[WGdZ5qZmLw0]', '[MAEM4xQRex5A]', '[]', '[]'], features)
        self.assertAllEqual(['WGdZ5qZmLw0', 'B2JO1owWbeLn', 'oQvJGPZAq8m', 'Dm6Z7kZn3MYm', 'E8NMJaXRqGGZ'], labels)

    def test_generate(self):
        gen = CsvDataGenerator(CURRENT_DIR, batch_size=3).generate()
        batch1 = next(gen)
        self.assertAllEqual(['[]', '[WGdZ5qZmLw0]', '[MAEM4xQRex5A]'], batch1[0])
        self.assertAllEqual(['WGdZ5qZmLw0', 'B2JO1owWbeLn', 'oQvJGPZAq8m'], batch1[1])
        batch2 = next(gen)
        self.assertAllEqual(['[]', '[]'], batch2[0])
        self.assertAllEqual(['Dm6Z7kZn3MYm', 'E8NMJaXRqGGZ'], batch2[1])


class BaseTFCsvDataGeneratorTest(tf.test.TestCase):

    def test_preprocess(self):
        with self.test_session():
            record_defaults = [[""]] * 10
            gen = TFCsvDataGenerator(CURRENT_DIR, record_defaults, batch_size=3)
            records = gen.preprocess("108231", "2018-08-12T16:57:53.019Z", "SUNDAY", "EVENING", "[USA,UK]", "WGdZ5qZmLw0",
                                    "[a,b,c,d,e,f]", "[i,j,k,l]", "[David Yates]", "[abc]")
            features = records[0]
            labels = records[1]
            watched = features['watched']
            day_of_week = features['day_of_week']
            day_period = features['day_period']
            watch_category = features['watch_category']
            watch_actor = features['watch_actor']
            watch_country = features['watch_country']
            watch_director = features['watch_director']
            self.assertAllEqual([b'', b'', b'', b'', b'abc'], watched)
            self.assertAllEqual(['SUNDAY'], day_of_week)
            self.assertAllEqual(['EVENING'], day_period)
            self.assertAllEqual([b'', b'i', b'j', b'k', b'l'], watch_category)
            self.assertAllEqual([b'b', b'c', b'd', b'e', b'f'], watch_actor)
            self.assertAllEqual([b'', b'', b'', b'USA', b'UK'], watch_country)
            self.assertAllEqual([b'', b'', b'', b'', b'David Yates'], watch_director)

            self.assertAllEqual([1], labels)

    def test_generate(self):
        with self.test_session() as sess:
            record_defaults = [[""]] * 10
            gen = TFCsvDataGenerator(CURRENT_DIR, record_defaults, batch_size=10).generate(is_train=False, num_parallel_calls=1)
            sess.run(gen.initializer)
            batch = gen.get_next()
            features = batch[0]
            actual_labels = batch[1]
            actual_watched = features['watched']
            actual_day_of_week = features['day_of_week']
            actual_day_period = features['day_period']
            actual_watch_category = features['watch_category']
            actual_watch_actor = features['watch_actor']
            actual_watch_country = features['watch_country']
            actual_watch_director = features['watch_director']

            exp_watched = [[b'', b'', b'', b'', b''], [b'', b'', b'', b'', b'WGdZ5qZmLw0'],
                         [b'', b'', b'', b'', b'MAEM4xQRex5A'], [b'', b'', b'', b'', b''], [b'', b'', b'', b'', b'']]
            exp_day_of_week = [[b'SUNDAY'], [b'SUNDAY'], [b'SUNDAY'], [b'SATURDAY'], [b'SATURDAY']]
            exp_day_period = [[b'EVENING'], [b'EVENING'], [b'EARLY_MORNING'], [b'NIGHT'], [b'EVENING']]
            exp_watch_category = [[b'', b'i', b'j', b'k', b'l'], [b'', b'', b'', b'action', b'adventure'],
                                  [b'', b'', b'', b'', b'comedy'], [b'', b'', b'', b'action', b'adventure'],
                                  [b'', b'', b'adventure', b'family', b'fantasy']]
            exp_watch_actor = [[b'b', b'c', b'd', b'e', b'f'], [b'', b'', b'Robert Downey Jr.', b'Chris Hemsworth',
                                                                b'Mark Ruffalo'],
                               [b'', b'', b'Kim Soo Mi', b'Jeong Man Sik', b'Kim Jung Tae'],
                               [b'', b'Robert Downey Jr.', b'Chris Hemsworth', b'Mark Ruffalo', b'Chris Evans'],
                               [b'', b'Oprah Winfrey', b'Reese Witherspoon', b'Mindy Kaling', b'Chris Pine']]
            exp_watch_country = [[b'', b'', b'', b'USA', b'UK'], [b'', b'', b'', b'', b'USA'],
                                 [b'', b'', b'', b'', b'Korea'], [b'', b'', b'', b'', b'USA'],
                                 [b'', b'', b'', b'', b'USA']]
            exp_watch_director = [[b'', b'', b'', b'', b'David Yates'], [b'', b'', b'', b'Anthony Russo', b'Joe Russo'],
                                  [b'', b'', b'', b'', b'Shin Han Sol'], [b'', b'', b'', b'Anthony Russo', b'Joe Russo'],
                                  [b'', b'', b'', b'', b'Ava DuVernay']]
            exp_labels = [[1], [1], [1], [1], [1]]
            self.assertAllEqual(exp_watched, actual_watched)
            self.assertAllEqual(exp_day_of_week, actual_day_of_week)
            self.assertAllEqual(exp_day_period, actual_day_period)
            self.assertAllEqual(exp_watch_category, actual_watch_category)
            self.assertAllEqual(exp_watch_actor, actual_watch_actor)
            self.assertAllEqual(exp_watch_country, actual_watch_country)
            self.assertAllEqual(exp_watch_director, actual_watch_director)
            self.assertAllEqual(exp_labels, actual_labels)


class UtilsTest(tf.test.TestCase):

    def test_string_to_tensor(self):
        with self.test_session():
            x = tf.constant("[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg,4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]")
            actual = string_to_tensor(x).eval()
            exp = [[b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg', b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg']]
            self.assertAllEqual(actual, exp)

    def test_string_to_tensor_empty_list(self):
        with self.test_session():
            x = tf.constant("[]")
            actual = string_to_tensor(x).eval()
            exp = [[]]
            self.assertAllEqual(actual, exp)

    def test_pad_string_tensor_empty_list(self):
        with self.test_session():
            x = tf.constant("[]")
            actual = pad_string_tensor(x, 3).eval()
            exp = [b'', b'', b'']
            self.assertAllEqual(actual, exp)

    def test_pad_string_tensor_single_element(self):
        with self.test_session():
            x = tf.constant("[xxx]")
            actual = pad_string_tensor(x, 3).eval()
            exp = [b'', b'', b'xxx']
            self.assertAllEqual(actual, exp)

    def test_pad_string_tensor_seq_less_than_maxlen(self):
        with self.test_session():
            x = tf.constant("[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg,4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]")
            actual = pad_string_tensor(x, 10).eval()
            exp = [b'', b'', b'', b'', b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg', b'4QaedmvnE94', b'Nm6mdRQmPMW',
                   b'MdXNpLQe3AOg']
            self.assertAllEqual(actual, exp)

    def test_pad_string_tensor_seq_equal_maxlen(self):
        with self.test_session():
            x = tf.constant("[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg,4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]")
            actual = pad_string_tensor(x, 6).eval()
            exp = [b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg', b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg']
            self.assertAllEqual(actual, exp)

    def test_pad_string_tensor_seq_greater_than_maxlen(self):
        with self.test_session():
            x = tf.constant("[4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg,4QaedmvnE94,Nm6mdRQmPMW,MdXNpLQe3AOg]")
            actual = pad_string_tensor(x, 3).eval()
            exp = [b'4QaedmvnE94', b'Nm6mdRQmPMW', b'MdXNpLQe3AOg']
            self.assertAllEqual(actual, exp)


if __name__ == '__main__':
    tf.test.main()