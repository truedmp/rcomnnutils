import pandas as pd
import numpy as np
from unittest import TestCase
from dmputils.job_stat import _flatten_list, _get_most_common_in_list,\
    _top_value_count, _total_empty_list, _unexpected_item_in_df, _unexpected_item_in_list,\
    _get_unexpected, _get_numeric_stat, _generate_dict, _convert_dict_json


class TestJobStat(TestCase):

    def test_flatten_list_listtype(self):
        df = pd.DataFrame(
            [{
                'id': 'id1',
                'actors': ['a', 'b', 'c']
            }, {
                'id': 'id2',
                'actors': ['a', 'b', 'c', 'd']
            }])
        actual = _flatten_list(df, 'actors')
        expect = list(['a', 'b', 'c', 'a', 'b', 'c', 'd'])
        self.assertEqual(expect, actual)

    def test_get_most_common_in_list_n1(self):
        df = pd.DataFrame(
            [{
                'id': 'id1',
                'tags': ['a', 'b', 'c']
            }, {
                'id': 'id2',
                'tags': ['a', 'b', 'c', 'd']
            }, {
                'id': 'id3',
                'tags': ['a', 'a', 'a']
            }])
        actual = _get_most_common_in_list(df, 'tags', 1)
        expect = (['a'], [5], [50.0])
        self.assertEqual(expect, actual)

    def test_get_most_common_in_list_n3(self):
        df = pd.DataFrame(
            [{
                'id': 'id1',
                'tags': ['a', 'b', 'c']
            }, {
                'id': 'id2',
                'tags': ['a', 'b', 'c', 'd']
            }, {
                'id': 'id3',
                'tags': ['a', 'a', 'a']
            }])
        actual = _get_most_common_in_list(df, 'tags', 3)
        expect = (['a', 'b', 'c'], [5, 2, 2], [50.0, 20.0, 20.0])
        self.assertEqual(expect, actual)

    def test_get_most_common_in_list_n5(self):
        df = pd.DataFrame(
            [{
                'id': 'id1',
                'tags': ['a', 'b', 'c']
            }, {
                'id': 'id2',
                'tags': ['a', 'b', 'c', 'd']
            }, {
                'id': 'id3',
                'tags': ['a', 'a', 'a']
            }])
        actual = _get_most_common_in_list(df, 'tags', 6)
        expect = (['a', 'b', 'c', 'd'], [5, 2, 2, 1], [50.0, 20.0, 20., 10.0])
        self.assertEqual(expect, actual)

    def test_top_value_count(self):
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_type': 'cash'
            }, {
                'id': 'ddd',
                'redeem_type': 'point'
            }, {
                'id': 'eee',
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_type': 'cash'
            }])
        actual = _top_value_count(df, 'redeem_type')
        expect = (['cash', 'point'], [4, 2], [66.67, 33.33])
        self.assertEqual(expect, actual)

    def test_total_empty_list(self):
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'test_tags': ['a', 'b', 'c']
            }, {
                'id': 'bbb',
                'test_tags': ['a', 'b', 'c', 'd']
            }, {
                'id': 'ccc',
                'test_tags': []
            }, {
                'id': 'ddd',
                'test_tags': ['c', 'd', 'e', 'd', 'z']
            }, {
                'id': 'eee',
                'test_tags': ['a', 's', 's', 's']
            }, {
                'id': 'fff',
                'test_tags': ['z']
            }, {
                'id': 'ggg',
                'test_tags': []
            }])
        actual = _total_empty_list(df, 'test_tags')
        expect = 2
        self.assertEqual(expect, actual)

    def test_unexpected_item_in_list(self):
        white_list = {
            'test_tags': ['a', 'b', 'c', 'd'],
            'test_redeem_type': ['cash', 'point']
        }
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'test_tags': ['a', 'b', 'c']
            }, {
                'id': 'bbb',
                'test_tags': ['a', 'b', 'c', 'd']
            }, {
                'id': 'ccc',
                'test_tags': []
            }, {
                'id': 'ddd',
                'test_tags': ['c', 'd', 'e', 'd', 'z']
            }, {
                'id': 'eee',
                'test_tags': ['a', 's', 's', 's']
            }, {
                'id': 'fff',
                'test_tags': ['z']
            }, {
                'id': 'ggg',
                'test_tags': []
            }])
        actual_list = _unexpected_item_in_list(df, 'test_tags', white_list)
        zipped = sorted(zip(actual_list[0], actual_list[1], actual_list[2]), key=lambda x: x[1], reverse=True)
        item, count, percent = zip(*zipped)
        actual = (list(item), list(count), list(percent))
        expect = (['s', 'z', 'e'], [3, 2, 1], [17.65, 11.76, 5.88])
        self.assertEqual(expect, actual)

    def test_unexpected_item_in_df(self):
        white_list = {
            'test_tags': ['a', 'b', 'c', 'd'],
            'test_redeem_type': ['cash', 'point']
        }
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'test_redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'test_redeem_type': 'coupon'
            }, {
                'id': 'ccc',
                'test_redeem_type': 'point'
            }, {
                'id': 'ddd',
                'test_redeem_type': 'coupon'
            }, {
                'id': 'eee',
                'test_redeem_type': 'CASH'
            }, {
                'id': 'fff',
                'test_redeem_type': 'point'
            }, {
                'id': 'ggg',
                'test_redeem_type': np.nan
            }])

        actual_list = _unexpected_item_in_df(df, 'test_redeem_type', white_list)
        zipped = sorted(zip(actual_list[0], actual_list[1], actual_list[2]), key=lambda x: x[1], reverse=True)
        item, count, percent = zip(*zipped)
        actual = (list(item), list(count), list(percent))
        expect = (['coupon', 'CASH'], [2, 1], [33.33, 16.67])
        self.assertEqual(expect, actual)

    def test_get_unexpected_numeric(self):
        white_list = {
            'tags': ['a', 'b', 'c', 'd'],
            'redeem_type': ['cash', 'point']
        }
        dictionary = dict()
        dictionary['unexpectedItems'] = []
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': 32.4,
                'tags': ['c', 'd', 'e', 'd', 'z'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'tags': ['a', 's', 't'],
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'tags': [],
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'tags': [],
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        _get_unexpected(df, 'abc', 3, white_list, dictionary)
        actual = dictionary
        expect = {
            'unexpectedItems': []
            }
        self.assertEqual(expect, actual)

    def test_get_unexpected_list(self):
        white_list = {
            'tags': ['a', 'b', 'c', 'd'],
            'redeem_type': ['cash', 'point']
        }

        dictionary = dict()
        dictionary['unexpectedItems'] = []
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': 32.4,
                'tags': ['c', 'd', 'e', 'd'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'tags': ['a', 's', 't', 't'],
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'tags': [],
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'tags': ['e'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        _get_unexpected(df, 'tags', 3, white_list, dictionary)
        actual = dictionary
        expect = {
            'unexpectedItems': [{'name': 'tags', 'key': ['t', 'e', 's'],
                                'count': [3, 2, 1], 'percent': [15.0, 10.0, 5.0]}]
            }
        self.assertEqual(expect, actual)

    def test_get_unexpected_df(self):
        white_list = {
            'tags': ['a', 'b', 'c', 'd'],
            'redeem_type': ['cash', 'point']
        }
        dictionary = dict()
        dictionary['unexpectedItems'] = []
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': 32.4,
                'tags': ['c', 'd', 'e', 'd', 'z'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'tags': ['a', 's', 't'],
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'tags': [],
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'tags': [],
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        _get_unexpected(df, 'redeem_type', 3, white_list, dictionary)
        actual = dictionary
        expect = {
                'unexpectedItems': [{'name': 'redeem_type', 'key': ['poiNT', 'cashhhh'],
                                    'count': [2, 1], 'percent': [33.33, 16.67]}]
                }
        self.assertEqual(expect, actual)

    def test_get_unexpected_no_white_list(self):
        white_list = {
            'tags': ['a', 'b', 'c', 'd'],
            'test_redeem_type': ['cash', 'point']
        }
        dictionary = dict()
        dictionary['unexpectedItems'] = []
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': 32.4,
                'tags': ['c', 'd', 'e', 'd', 'z'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'tags': ['a', 's', 't'],
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'tags': [],
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'tags': [],
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        _get_unexpected(df, 'redeem_type', 3, white_list, dictionary)
        actual = dictionary
        expect = {
            'unexpectedItems': []
            }
        self.assertEqual(expect, actual)

    def test_get_unexpected_no_unexpect(self):
        white_list = {
            'tags': ['a', 'b', 'c', 'd'],
            'redeem_type': ['cash', 'point']
        }
        dictionary = dict()
        dictionary['unexpectedItems'] = []
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }])
        _get_unexpected(df, 'redeem_type', 3, white_list, dictionary)
        actual = dictionary
        expect = {
            'unexpectedItems': []
            }
        self.assertEqual(expect, actual)

    def test_get_numeric_stat_int(self):
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': 32.4,
                'tags': ['c', 'd', 'e', 'd', 'z'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'tags': ['a', 's', 't'],
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'tags': [],
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'tags': [],
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        actual = _get_numeric_stat(df, 'redeem_count')
        expect = (432434, 4, 151741.87, 57750.5, 4)
        self.assertEqual(expect, actual)

    def test_get_numeric_stat_float(self):
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 4.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': 32.4,
                'tags': ['c', 'd', 'e', 'd', 'z'],
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'tags': ['a', 's', 't'],
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'tags': [],
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'tags': [],
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        actual = _get_numeric_stat(df, 'rating')

        expect = (33443.342, 3.4, 12634.37, 4791.32, 4.2)
        self.assertEqual(expect, actual)

    def test_get_numeric_check_nan(self):
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 0,
                'redeem_type': 'point'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 4,
                'rating': np.nan,
                'redeem_type': 'poiNT'
            }, {
                'id': 'eee',
                'redeem_count': 4,
                'rating': 47.7,
                'redeem_type': np.nan
            }, {
                'id': 'fff',
                'redeem_count': 87,
                'rating': np.nan,
                'redeem_type': 'cash'
            }, {
                'id': 'ggg',
                'redeem_count': 40,
                'rating': 3.4,
                'redeem_type': 'poiNT'
            }, {
                'id': 'hhh',
                'redeem_count': 87,
                'rating': 4.2,
                'redeem_type': np.nan
            }])
        actual = _get_numeric_stat(df, 'rating')
        expect = (47.7, 0.0, 18.25, 10.58, 4.2)
        self.assertEqual(expect, actual)

    def test_generate_dict_white_list(self):
        white_list = {
              'test_tags': ['a', 'b', 'c', 'd'],
              'test_redeem_type': ['cash', 'point']
            }
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'cash'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        actual = _generate_dict('abc', df, 5, white_list)
        expect = {
             'column': ['id', 'rating', 'redeem_count', 'redeem_type', 'tags'],
             'df_name': 'abc',
             'emptyList': [{'name': 'tags', 'value': 1}],
             'freqItems': [{'count': [2, 1],
                            'key': ['cash', 'cashhhh'],
                            'name': 'redeem_type',
                            'percent': [66.67, 33.33]},
                           {'count': [3, 3, 2, 2, 1],
                            'key': ['a', 'c', 'b', 'd', 't'],
                            'name': 'tags',
                            'percent': [27.27, 27.27, 18.18, 18.18, 9.09]}],
             'max': [{'name': 'rating', 'value': 33443.342},
                     {'name': 'redeem_count', 'value': 432434}],
             'mean': [{'name': 'rating', 'value': 8373.69},
                      {'name': 'redeem_count', 'value': 115467.25}],
             'min': [{'name': 'rating', 'value': 4.0},
                     {'name': 'redeem_count', 'value': 6}],
             'mode': [{'name': 'rating', 'value': 4.0},
                      {'name': 'redeem_count', 'value': 6}],
             'null': [{'name': 'redeem_type', 'value': 1}],
             'row_count': 4,
             'schema': [{'name': 'id', 'value': 'string'},
                        {'name': 'rating', 'value': 'numeric'},
                        {'name': 'redeem_count', 'value': 'numeric'},
                        {'name': 'redeem_type', 'value': 'string'},
                        {'name': 'tags', 'value': 'list'}],
             'std': [{'name': 'rating', 'value': 16713.11},
                     {'name': 'redeem_count', 'value': 211761.96}],
             'unexpectedItems': []
            }
        self.assertEqual(expect, actual)

    def test_generate_dict_no_white_list(self):
        df = pd.DataFrame(
            [{
                'id': 'aaa',
                'redeem_count': 432434,
                'rating': 4.0,
                'tags': ['a', 'b', 'c'],
                'redeem_type': 'cash'
            }, {
                'id': 'bbb',
                'redeem_count': 29342,
                'rating': 43.2,
                'tags': ['a', 'b', 'c', 'd'],
                'redeem_type': 'cash'
            }, {
                'id': 'ccc',
                'redeem_count': 6,
                'rating': 4.2,
                'tags': [],
                'redeem_type': 'cashhhh'
            }, {
                'id': 'ddd',
                'redeem_count': 87,
                'rating': 33443.342,
                'tags': ['a', 'c', 'd', 't'],
                'redeem_type': np.nan
            }])
        actual = _generate_dict('abc', df, 5)
        expect = {
             'column': ['id', 'rating', 'redeem_count', 'redeem_type', 'tags'],
             'df_name': 'abc',
             'emptyList': [{'name': 'tags', 'value': 1}],
             'freqItems': [{'count': [2, 1],
                            'key': ['cash', 'cashhhh'],
                            'name': 'redeem_type',
                            'percent': [66.67, 33.33]},
                           {'count': [3, 3, 2, 2, 1],
                            'key': ['a', 'c', 'b', 'd', 't'],
                            'name': 'tags',
                            'percent': [27.27, 27.27, 18.18, 18.18, 9.09]}],
             'max': [{'name': 'rating', 'value': 33443.342},
                     {'name': 'redeem_count', 'value': 432434}],
             'mean': [{'name': 'rating', 'value': 8373.69},
                      {'name': 'redeem_count', 'value': 115467.25}],
             'min': [{'name': 'rating', 'value': 4.0},
                     {'name': 'redeem_count', 'value': 6}],
             'mode': [{'name': 'rating', 'value': 4.0},
                      {'name': 'redeem_count', 'value': 6}],
             'null': [{'name': 'redeem_type', 'value': 1}],
             'row_count': 4,
             'schema': [{'name': 'id', 'value': 'string'},
                        {'name': 'rating', 'value': 'numeric'},
                        {'name': 'redeem_count', 'value': 'numeric'},
                        {'name': 'redeem_type', 'value': 'string'},
                        {'name': 'tags', 'value': 'list'}],
             'std': [{'name': 'rating', 'value': 16713.11},
                     {'name': 'redeem_count', 'value': 211761.96}],
             'unexpectedItems': []
            }
        self.assertEqual(expect, actual)

    def test_convert_dict_json_check_type(self):
        dictionary = {
            'df_name': 'test',
            'column': [np.int64(1), np.int64(2), np.int64(3)],
            'row_count': np.int64(8),
            'null': [{'name': 'rating', 'value': 1}]
        }
        actual_dictionary = _convert_dict_json(dictionary)
        actual_int = isinstance(actual_dictionary['row_count'], int)
        expect_int = True
        actual_int64 = isinstance(actual_dictionary['row_count'], np.int64)
        expect_int64 = False
        self.assertEqual(expect_int, actual_int)
        self.assertEqual(expect_int64, actual_int64)

    def test_convert_dict_json_check_dictionary_result(self):
        dictionary = {
            'df_name': 'test',
            'column': [np.int64(1), np.int64(2), np.int64(3)],
            'row_count': np.int64(8),
            'null': [{'name': 'rating', 'value': 1}]
        }
        actual = _convert_dict_json(dictionary)
        expect = {
            'df_name': 'test',
            'column': [int(1), int(2), int(3)],
            'row_count': int(8),
            'null': [{'name': 'rating', 'value': 1}]
        }
        self.assertEqual(expect, actual)

