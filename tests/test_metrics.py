import tensorflow as tf
import numpy as np
from dmputils.metrics import ndcg_score, dcg_score, _mask_top_k, micro_f1_score, _compute_dcg_vector, getNearestIndex


class EvaluationMetricsTest(tf.test.TestCase):

    def test_getNearestIndex_cosine(self):
        embedding_vector = [[2, 2], [2, 0], [0, 2]]
        ground_truth = [[0, 5], [1, 0], [0, 5], [1, 0]]
        k = 2
        distance = 'cosine'

        with tf.Session() as sess:
            tf_emb = tf.convert_to_tensor(embedding_vector, dtype=tf.float32)
            y_true = tf.convert_to_tensor(ground_truth, dtype=tf.float32)
            y_true = tf.expand_dims(y_true, 1)
            _, sorted_true_rel_idx = getNearestIndex(
                tf_emb, y_true, k, distance)
            expect = sess.run(sorted_true_rel_idx)
            actual = np.array([[2, 0], [1, 0], [2, 0], [1, 0]])
            self.assertEqual(expect.tolist(), actual.tolist())

    def test_getNearestIndex_euclidean(self):
        embedding_vector = [[1, 1], [1, 0], [0, 1]]
        ground_truth = [[0, 1], [1, 0], [0, 1], [1, 0]]
        k = 2
        distance = 'euclidean'

        with tf.Session() as sess:
            tf_emb = tf.convert_to_tensor(embedding_vector, dtype=tf.float32)
            y_true = tf.convert_to_tensor(ground_truth, dtype=tf.float32)
            y_true = tf.expand_dims(y_true, 1)
            _, sorted_true_rel_idx = getNearestIndex(
                tf_emb, y_true, k, distance)
            expect = sess.run(sorted_true_rel_idx)
            actual = np.array([[2, 0], [1, 0], [2, 0], [1, 0]])
            self.assertEqual(expect.tolist(), actual.tolist())

    def test_ndcg_score_vector_perfect_ranking_cosine(self):
        embedding_vector = [[2, 2], [2, 0], [0, 2]]
        ground_truth = [[0, 5], [1, 0], [0, 5], [1, 0]]
        predictions = [[0, 2], [2, 0], [0, 2], [2, 0]]
        k = 2
        distance = 'cosine'

        with tf.Session() as sess:
            tf_emb = tf.convert_to_tensor(embedding_vector, dtype=tf.float32)
            y_true = tf.convert_to_tensor(ground_truth, dtype=tf.float32)
            y_pred = tf.convert_to_tensor(predictions, dtype=tf.float32)
            score = _compute_dcg_vector(y_true, y_pred, k, distance, tf_emb)
            expect = sess.run(score)
            self.assertEqual(expect, 4.0)

    def test_ndcg_score_vector_imperfect_ranking_cosine(self):
        embedding_vector = [[2, 2], [2, 0], [0, 2]]
        ground_truth = [[0, 5], [1, 0], [0, 5], [1, 0]]
        predictions = [[5, 0], [0, 2], [2, 0], [0, 2]]
        k = 2
        distance = 'cosine'

        with tf.Session() as sess:
            tf_emb = tf.convert_to_tensor(embedding_vector, dtype=tf.float32)
            y_true = tf.convert_to_tensor(ground_truth, dtype=tf.float32)
            y_pred = tf.convert_to_tensor(predictions, dtype=tf.float32)
            score = _compute_dcg_vector(y_true, y_pred, k, distance, tf_emb)
            expect = sess.run(score)
            self.assertEqual(expect, 0.0)

    def test_ndcg_score_vector_perfect_ranking_euclidean(self):
        embedding_vector = [[1, 1], [1, 0], [0, 1]]
        ground_truth = [[0, 1], [1, 0], [0, 1], [1, 0]]
        predictions = [[0, 1], [1, 0], [0, 1], [1, 0]]
        k = 2
        distance = 'euclidean'

        with tf.Session() as sess:
            tf_emb = tf.convert_to_tensor(embedding_vector, dtype=tf.float32)
            y_true = tf.convert_to_tensor(ground_truth, dtype=tf.float32)
            y_pred = tf.convert_to_tensor(predictions, dtype=tf.float32)
            score = _compute_dcg_vector(y_true, y_pred, k, distance, tf_emb)
            expect = sess.run(score)
            self.assertEqual(expect, 4.0)

    def test_ndcg_score_vector_imperfect_ranking_euclidean(self):
        embedding_vector = [[1, 1], [1, 0], [0, 1]]
        ground_truth = [[0, 1], [1, 0], [0, 1], [1, 0]]
        predictions = [[1, 0], [0, 1], [1, 0], [0, 1]]
        k = 2
        distance = 'euclidean'

        with tf.Session() as sess:
            tf_emb = tf.convert_to_tensor(embedding_vector, dtype=tf.float32)
            y_true = tf.convert_to_tensor(ground_truth, dtype=tf.float32)
            y_pred = tf.convert_to_tensor(predictions, dtype=tf.float32)
            score = _compute_dcg_vector(y_true, y_pred, k, distance, tf_emb)
            expect = sess.run(score)
            self.assertEqual(expect, 0.0)

    def test_ndcg_score_perfect_ranking(self):
        ground_truth = [1, 0, 2]
        predictions = [[0.15, 0.55, 0.2], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = ndcg_score(ground_truth, predictions, k=5)
        self.assertEqual(score, 1.0)

    def test_ndcg_score_no_correct_ranking(self):
        ground_truth = [0, 2, 1]
        predictions = [[0.15, 0.55, 0.2], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = ndcg_score(ground_truth, predictions, k=1)
        self.assertEqual(score, 0.)

    def test_ndcg_score_2_missed_position_ranking(self):
        ground_truth = [1, 0, 1]
        predictions = [[0.15, 0.55, 0.2], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = ndcg_score(ground_truth, predictions, k=5)
        self.assertAlmostEqual(score, 0.83, places=2)

    def test_ndcg_score_1_missed_position_ranking(self):
        ground_truth = [1, 0, 0]
        predictions = [[0.15, 0.55, 0.2], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = ndcg_score(ground_truth, predictions, k=5)
        self.assertAlmostEqual(score, 0.88, places=2)

    def test_ndcg_score_worst_ranking2(self):
        ground_truth = [1, 0, 0]
        predictions = [[0.9, 0.5, 0.8], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = ndcg_score(ground_truth, predictions, k=2)
        self.assertAlmostEqual(score, 0.54, places=2)

    def test_ndcg_score_worst_ranking3(self):
        ground_truth = [2, 2, 2]
        predictions = [[0.9, 0.5, 0.8], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = ndcg_score(ground_truth, predictions, k=5)
        self.assertAlmostEqual(score, 0.71, places=2)

    def test_ndcg_score_invalid_predictions(self):
        ground_truth = [2, 2, 2]
        predictions = [[0.9, 0.5], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        with self.assertRaises(ValueError):
            ndcg_score(ground_truth, predictions, k=5)

    def test_ndcg_score_invalid_ground_truth(self):
        ground_truth = [2, 2]
        predictions = [[0.9, 0.5, 0.8], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        with self.assertRaises(ValueError):
            ndcg_score(ground_truth, predictions, k=5)

    def test_dcg_score(self):
        ground_truth = [2, 2, 2]
        predictions = [[0.9, 0.5, 0.8], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        score = dcg_score(ground_truth, predictions, k=5)
        self.assertAlmostEqual(score, 19.18, places=2)

    def test_dcg_score_too_low_probability(self):
        # this is very rare, Python interpreter interprets too low probability e.g. 1e-20 as zero.
        ground_truth = [0, 0, 0]
        predictions = [[1e-18, 1e-19, 1e-20],
                       [1e-18, 1e-19, 1e-20], [1e-18, 1e-19, 1e-20]]
        score = dcg_score(ground_truth, predictions, k=1)
        self.assertEqual(score, 0.)

    def test_dcg_score_invalid_predictions(self):
        ground_truth = [2, 2, 2]
        predictions = [[0.9, 0.5], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        with self.assertRaises(ValueError):
            dcg_score(ground_truth, predictions, k=5)

    def test_dcg_score_invalid_ground_truth(self):
        ground_truth = [2, 2]
        predictions = [[0.9, 0.5, 0.8], [0.7, 0.2, 0.1], [0.06, 0.04, 0.9]]
        with self.assertRaises(ValueError):
            dcg_score(ground_truth, predictions, k=5)

    def test_mask_top_k(self):
        with self.test_session():
            input_array = [[0, .8, .5, 0]]
            expected = [[0., 1., 1., 0.]]

            masked_array = _mask_top_k(input_array, k=2).eval().tolist()
            self.assertListEqual(masked_array, expected)

    def test_mask_top_k_with_two_samples(self):
        with self.test_session():
            input_array = [[0, .3, .5, .7], [0, .2, .1, .8]]
            expected = [[0, 0, 1, 1], [0, 1, 0, 1]]

            masked_array = _mask_top_k(input_array, k=2).eval().tolist()
            self.assertListEqual(masked_array, expected)

    def test_mask_top_k_with_k_is_3(self):
        with self.test_session():
            input_array = [[0, .3, .5, .7], [0, .2, .1, .8]]
            expected = [[0, 1, 1, 1], [0, 1, 1, 1]]

            masked_array = _mask_top_k(input_array, k=3).eval().tolist()
            self.assertListEqual(masked_array, expected)

    def test_mask_top_k_with_default_k(self):
        with self.test_session():
            input_array = [[0, 0, .3, .5, .7], [0, 0, .2, .1, .8]]
            expected = [[1, 1, 1, 1, 1], [1, 1, 1, 1, 1]]

            masked_array = _mask_top_k(input_array).eval().tolist()
            self.assertListEqual(masked_array, expected)

    def test_mask_top_k_raise_error_when_k_is_greater_than_input(self):
        with self.test_session():
            input_array = [[.3, .5, .7], [.2, .1, .8]]
            with self.assertRaises(ValueError):
                _mask_top_k(input_array).eval()

    def test_micro_f1_score_with_default_k_value(self):
        with self.test_session():
            predictions = [[0, 0, 0, 0, 1, 1]]
            ground_truth = [[0, 0, 1, 1, 1, 1]]

            expected = 0.800

            score = micro_f1_score(ground_truth, predictions).eval()
            self.assertAlmostEqual(score, expected, places=2)

    def test_micro_f1_score_with_k_is_2(self):
        with self.test_session():
            predictions = [[0, 0.7, 0.8, 0.9], [0.7, 0, 0.3, 0.9]]
            ground_truth = [[0, 1, 1, 1], [0, 1, 1, 1]]

            expected = 0.600

            score = micro_f1_score(ground_truth, predictions, k=2).eval()
            self.assertAlmostEqual(score, expected, places=2)

    def test_micro_f1_should_raise_error_when_shape_is_not_equal(self):
        with self.test_session():
            predictions = [[0, 0.7, 0.8, 0.9]]
            ground_truth = [[0, 1, 1, 1], [0, 1, 1, 1]]

            with self.assertRaises(Exception):
                micro_f1_score(ground_truth, predictions)


if __name__ == '__main__':
    tf.test.main()
